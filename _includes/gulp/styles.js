/**
 *  This file contains the variables used in other gulp files
 *  which defines tasks
 *  By design, we only put there very generic config values
 *  which are used in several places to keep good readability
 *  of the tasks
 */
var gulp = require('gulp'),
    $ = require('gulp-load-plugins')(),
    conf = require('./conf'),
    minifyCSS = require('gulp-minify-css');


/**
 *  Common implementation for an error handler of a Gulp plugin
 */
exports.sass = function(source, outputFolder, outputFileName) {
    'use strict';

    return gulp.src(source)
        .pipe($.sourcemaps.init())
        .pipe($.sass({ style: 'expanded' })).on('error', conf.errorHandler('Sass'))
        .pipe($.autoprefixer()).on('error', conf.errorHandler('Autoprefixer'))
        .pipe($.sourcemaps.write())
        .pipe($.rename(outputFileName + '.sourcemap.css'))
        .pipe(gulp.dest(outputFolder))
        .pipe($.stripCssComments())
        // no sourcemap
        .pipe($.rename(outputFileName + '.css'))
        .pipe(gulp.dest(outputFolder));

    //// min
    //    try{
    //
    //        .pipe($.rename(outputFileName + '.sourcemap.min.css'))
    //            .pipe(minifyCSS())
    //            .pipe($.sourcemaps.write())
    //            .pipe(gulp.dest(outputFolder))
    //            // min no source map
    //            .pipe($.stripCssComments())
    //            .pipe($.rename(outputFileName + '.min.css'))
    //            .pipe(gulp.dest(outputFolder));
    //    }catch (e){
    //
    //    }

};
