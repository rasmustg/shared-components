'use strict';

var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var markdown = require('gulp-markdown');
var markdownDocs = require('gulp-markdown-docs');
var styles = require('../../_includes/gulp/styles');


var sassFileToWatch = [__dirname + '\\sh-button.scss', __dirname + '\\sh-button.default.theme.scss'];

gulp.task('sh-button::styles', function() {

    var output = __dirname + '\\dist';

    styles.sass([__dirname + '\\sh-button.scss'], output, 'sh-button');
    styles.sass([__dirname + '\\sh-button.default.theme.scss'], output, 'sh-button.default.theme');

});

// http://weblogs.asp.net/dwahlin/creating-a-typescript-workflow-with-gulp
// https://github.com/ivogabe/gulp-typescript
// https://www.npmjs.com/package/gulp-type
// https://www.npmjs.com/package/gulp-markdown-docs

gulp.task('sh-button::markdown', function () {
    //return gulp.src('README.md')
    //    .pipe(markdown())
    //    .pipe($.rename('index.html'))
    //    .pipe(gulp.dest('.'));

    return gulp.src('README.md')
        .pipe(markdownDocs('index.html', {
            yamlMeta: false,
            templatePath: __dirname + '../../../_includes/index.html',
            layoutStylesheetUrl: __dirname + '../../../_includes/layout.css'
        }))
.pipe(gulp.dest('.'));

});

gulp.task('sh-button::default', function () {

    gulp.start('sh-button::styles');
    gulp.start('sh-button::markdown');

});

gulp.task('sh-button::watch', function () {

    gulp.watch(sassFileToWatch, ['sh-button::styles']);

});


//
//exports.shTypography = function() {
//    'use strict';
//
//    return function() {
//        gulp.start('sh-button::default');
//    };
//};


module.exports = function (gulp, plugins) {
    return function() {
        gulp.start('sh-button::default');
    };
};