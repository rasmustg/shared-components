'use strict';

var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var markdown = require('gulp-markdown');
var markdownDocs = require('gulp-markdown-docs');
var styles = require('../../_includes/gulp/styles');

gulp.task('sh-html-structure::styles', function() {

    var output = __dirname + '\\dist';
    var input = [__dirname + '\\*.scss'];

    return styles.sass(input, output, 'sh-html-structure');

});

// http://weblogs.asp.net/dwahlin/creating-a-typescript-workflow-with-gulp
// https://github.com/ivogabe/gulp-typescript
// https://www.npmjs.com/package/gulp-type
// https://www.npmjs.com/package/gulp-markdown-docs

gulp.task('sh-html-structure::markdown', function () {
    //return gulp.src('README.md')
    //    .pipe(markdown())
    //    .pipe($.rename('index.html'))
    //    .pipe(gulp.dest('.'));

    return gulp.src('README.md')
        .pipe(markdownDocs('index.html', {
            yamlMeta: false,
            templatePath: __dirname + '../../../_includes/index.html',
            layoutStylesheetUrl: __dirname + '../../../_includes/layout.css'
        }))
.pipe(gulp.dest('.'));

});

gulp.task('sh-html-structure::default', function () {

    gulp.start('sh-html-structure::styles');
    gulp.start('sh-html-structure::markdown');

});

module.exports = function (gulp, plugins) {
    return function() {
        gulp.start('sh-html-structure::default');
    };
};
