'use strict';

var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var markdown = require('gulp-markdown');
var markdownDocs = require('gulp-markdown-docs');
var styles = require('../../_includes/gulp/styles');

gulp.task('sh-icons::styles', function() {

    var output = __dirname + '\\dist';

    styles.sass([__dirname + '\\shureIcons.scss'], output, 'sh-icons');

});

// http://weblogs.asp.net/dwahlin/creating-a-typescript-workflow-with-gulp
// https://github.com/ivogabe/gulp-typescript
// https://www.npmjs.com/package/gulp-type
// https://www.npmjs.com/package/gulp-markdown-docs

gulp.task('sh-icons::markdown', function () {
    //return gulp.src('README.md')
    //    .pipe(markdown())
    //    .pipe($.rename('index.html'))
    //    .pipe(gulp.dest('.'));

    return gulp.src('README.md')
        .pipe(markdownDocs('index.html', {
            yamlMeta: false,
            templatePath: __dirname + '../../../_includes/index.html',
            layoutStylesheetUrl: __dirname + '../../../_includes/layout.css'
        }))
.pipe(gulp.dest('.'));

});

gulp.task('sh-icons::default', function () {

    gulp.start('sh-icons::styles');
    gulp.start('sh-icons::markdown');

});

//
//exports.shTypography = function() {
//    'use strict';
//
//    return function() {
//        gulp.start('sh-icons::default');
//    };
//};


module.exports = function (gulp, plugins) {
    return function() {
        gulp.start('sh-icons::default');
    };
};