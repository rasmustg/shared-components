'use strict';

var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var markdown = require('gulp-markdown');
var markdownDocs = require('gulp-markdown-docs');
var styles = require('../../_includes/gulp/styles');

gulp.task('sh-typography::styles', function() {

    var output = __dirname + '\\dist';

    styles.sass([__dirname + '\\sh-typography.scss'], output, 'sh-typography');
    styles.sass([__dirname + '\\sh-typography.default.theme.scss'], output, 'sh-typography.default.theme');

});

// http://weblogs.asp.net/dwahlin/creating-a-typescript-workflow-with-gulp
// https://github.com/ivogabe/gulp-typescript
// https://www.npmjs.com/package/gulp-type
// https://www.npmjs.com/package/gulp-markdown-docs

gulp.task('sh-typography::markdown', function () {
    //return gulp.src('README.md')
    //    .pipe(markdown())
    //    .pipe($.rename('index.html'))
    //    .pipe(gulp.dest('.'));

    return gulp.src('README.md')
        .pipe(markdownDocs('index.html', {
            yamlMeta: false,
            templatePath: __dirname + '../../../_includes/index.html',
            layoutStylesheetUrl: __dirname + '../../../_includes/layout.css'
        }))
.pipe(gulp.dest('.'));

});

gulp.task('sh-typography::default', function () {

    gulp.start('sh-typography::styles');
    gulp.start('sh-typography::markdown');

});

module.exports = function (gulp, plugins) {
    return function() {
        gulp.start('sh-typography::default');
    };
};