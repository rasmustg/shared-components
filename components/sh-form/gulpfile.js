'use strict';

var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var markdown = require('gulp-markdown');
var markdownDocs = require('gulp-markdown-docs');
var styles = require('../../_includes/gulp/styles');


var sassFileToWatch = [__dirname + '\\*.scss'];

gulp.task('sh-form::styles', function() {

    var output = __dirname + '\\dist';

    styles.sass([__dirname + '\\sh-form.scss'], output, 'sh-form');
    styles.sass([__dirname + '\\sh-form.default.theme.scss'], output, 'sh-form.default.theme');

});

// http://weblogs.asp.net/dwahlin/creating-a-typescript-workflow-with-gulp
// https://github.com/ivogabe/gulp-typescript
// https://www.npmjs.com/package/gulp-type
// https://www.npmjs.com/package/gulp-markdown-docs

gulp.task('sh-form::markdown', function () {
    //return gulp.src('README.md')
    //    .pipe(markdown())
    //    .pipe($.rename('index.html'))
    //    .pipe(gulp.dest('.'));

    return gulp.src('README.md')
        .pipe(markdownDocs('index.html', {
            yamlMeta: false,
            templatePath: __dirname + '../../../_includes/index.html',
            layoutStylesheetUrl: __dirname + '../../../_includes/layout.css'
        }))
.pipe(gulp.dest('.'));

});

gulp.task('sh-form::default', function () {

    gulp.start('sh-form::styles');
    gulp.start('sh-form::markdown');

});

gulp.task('sh-form::watch', function () {

    gulp.watch(sassFileToWatch, ['sh-form::styles']);

});


//
//exports.shTypography = function() {
//    'use strict';
//
//    return function() {
//        gulp.start('sh-form::default');
//    };
//};


module.exports = function (gulp, plugins) {
    return function() {
        gulp.start('sh-form::default');
    };
};