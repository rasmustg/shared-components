angular.module('shure.sh-list', ['perfect_scrollbar']);

angular.module('shure.sh-list')
    .config([function () {

    }]);

angular.module('shure.sh-list')
    .run([function () {

    }]);

angular.module('shure.sh-list')
    .controller('shListAppCtrl', ['$scope',
        function ($scope) {
            var vm = this;
            vm.name = 'rasmus';

            vm.users = [];

            for(var i=0; i<100; i++){
                vm.users.push({
                    firstname:'Firstname '+ i,
                    lastname:'Lastname '+ i,
                    age:i
                });
            }
        }]);


(function () {
// http://www.angularjshub.com/examples/customdirectives/compilelinkfunctions/
    angular
        .module('shure.sh-list')
        .directive('shListStickyHeader', shListStickyHeader);

    shListStickyHeader.$inject = ['$compile', '$window'];
    function shListStickyHeader($compile, $window){
        var directive = {
            restrict: 'A',
            scope: {
                max: '=',
                users: '='
            },
            //replace:true,
            template:'<div class="sh-list" ng-transclude></ng-table>',
            transclude: true,
            link: linkFunc,
            controller: shListStickyHeaderController,
            controllerAs: 'shListStickyHeaderVm',
            bindToController: true // because the scope is isolated
        };

        /*
         <div class="sh-list-header sh-list-header-sticky">
         <table class="sh-list sh-list-sticky-header"><thead><tr><th></th><th>Firstname</th><th>Lastname</th><th></th></tr></thead></table></div>
         */

        // Initialization
        console.log('init')

        return directive;

        function linkFunc(scope, element, attrs, ctrl, transcludeFn) {
            /* */
            console.log('LINK: scope.min = %s *** should be undefined', scope.min);
            console.log('LINK: scope.max = %s *** should be undefined', scope.max);
            console.log('LINK: scope.vm.min = %s', scope.shListStickyHeaderVm.min);
            console.log('LINK: scope.vm.max = %s', scope.shListStickyHeaderVm.max);

            var table = angular.copy(element);
            table.find('tbody').remove();
            table.find('tfoot').remove();
            // VERY important, otherwise we
            table.removeAttr('sh-list-sticky-header');
            var outer = $('<div class="sh-list-header sh-list-header-sticky"></div>');
            outer.html(table);
            element.closest('.sh-view').before($compile(outer)(scope));
        }
    }
    var windowThrottle;
    shListStickyHeaderController.$inject = ['$scope', '$element', '$attrs', '$window', '$timeout'];
    function shListStickyHeaderController($scope, $element, $attrs, $window, $timeout) {
        // Injecting $scope just for comparison
        var vm = this;

        vm.min = 3;

        console.log('CTRL: $scope.vm.min = %s', $scope.shListStickyHeaderVm.min);
        console.log('CTRL: $scope.vm.max = %s', $scope.shListStickyHeaderVm.max);
        console.log('CTRL: vm.min = %s', vm.min);
        console.log('CTRL: vm.max = %s', vm.max);

        vm.size = {
            height : $element[0].clientHeight,
            width : $element[0].clientWidth
        };
        function onResize() {
            if(windowThrottle){
                $timeout.cancel(windowThrottle);
            }
            windowThrottle = $timeout(function() {

                vm.size = {
                    height : $element[0].clientHeight,
                    width : $element[0].clientWidth
                };
                console.log(vm.size);
            }, 100);
        };

        angular.element($window).bind('resize', onResize);
        $scope.$on('$destroy', function() {
            angular.element($window).unbind('resize', onResize);
        });
    }
}());
