'use strict';

var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var markdown = require('gulp-markdown');
var markdownDocs = require('gulp-markdown-docs');
var styles = require('../../_includes/gulp/styles');


var sassFileToWatch = [__dirname + '\\sh-list.scss', __dirname + '\\sh-list.default.theme.scss'];

gulp.task('sh-list::styles', function() {

    var output = __dirname + '\\dist';

    styles.sass([__dirname + '\\sh-list.scss'], output, 'sh-list');
    styles.sass([__dirname + '\\sh-list.default.theme.scss'], output, 'sh-list.default.theme');

});

// http://weblogs.asp.net/dwahlin/creating-a-typescript-workflow-with-gulp
// https://github.com/ivogabe/gulp-typescript
// https://www.npmjs.com/package/gulp-type
// https://www.npmjs.com/package/gulp-markdown-docs

gulp.task('sh-list::markdown', function () {
    //return gulp.src('README.md')
    //    .pipe(markdown())
    //    .pipe($.rename('index.html'))
    //    .pipe(gulp.dest('.'));

    return gulp.src('README.md')
        .pipe(markdownDocs('index.html', {
            yamlMeta: false,
            templatePath: __dirname + '../../../_includes/index.html',
            layoutStylesheetUrl: __dirname + '../../../_includes/layout.css'
        }))
.pipe(gulp.dest('.'));

});

gulp.task('sh-list::default', function () {

    gulp.start('sh-list::styles');
    gulp.start('sh-list::markdown');

});

gulp.task('sh-list::watch', function () {

    gulp.watch(sassFileToWatch, ['sh-list::styles']);

});


//
//exports.shTypography = function() {
//    'use strict';
//
//    return function() {
//        gulp.start('sh-list::default');
//    };
//};


module.exports = function (gulp, plugins) {
    return function() {
        gulp.start('sh-list::default');
    };
};