/**
 *  Welcome to your gulpfile!
 *  The gulp tasks are splitted in several files in the gulp directory
 *  because putting all here was really too long
 */

'use strict';

var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();
var requireDir = require('require-dir');

//
///**
// *  This will load all js or coffee files in the gulp directory
// *  in order to load all gulp tasks
// */
//wrench.readdirSyncRecursive('./components/**/').filter(function(file) {
//    return (/\.(js|coffee)$/i).test(file);
//}).map(function(file) {
//    require('./gulp/' + file);
//});



function getComponent(component) {
    return require('./components/' + component + '/gulpfile')(gulp, plugins);
}


gulp.task('sh-button', getComponent('sh-button'));
gulp.task('sh-html-structure', getComponent('sh-html-structure'));
gulp.task('sh-typography', getComponent('sh-typography'));
gulp.task('sh-form', getComponent('sh-form'));
gulp.task('sh-icons', getComponent('sh-icons'));
gulp.task('sh-list', getComponent('sh-list'));



/**
 *  Default task clean temporaries directories and launch the
 *  main optimization build task
 */
gulp.task('default',  function () {
    gulp.start('sh-button');
    gulp.start('sh-html-structure');
    gulp.start('sh-typography');
    gulp.start('sh-form');
    gulp.start('sh-icons');
    gulp.start('sh-list');
});
